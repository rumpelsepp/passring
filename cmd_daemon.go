package main

import (
	"git.sr.ht/~rumpelsepp/passring/lib/service"
	"github.com/spf13/cobra"
)

type daemonCommand struct {
	globalOpts *globalOptions
}

func (c *daemonCommand) run(cmd *cobra.Command, args []string) error {
	s, err := service.NewSecretService(c.globalOpts.collection)
	if err != nil {
		return err
	}

	ctx := s.Conn.Context()

	select {
	case <-ctx.Done():
		if err := ctx.Err(); err != nil {
			cmd.PrintErrln(err)
		}
	}

	return nil
}
