package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

type collectionCommand struct {
	globalOpts *globalOptions
	init       bool
}

func (c *collectionCommand) run(cmd *cobra.Command, args []string) error {
	if len(args) != 1 {
		return fmt.Errorf("collection name is missing")
	}

	if c.init {
		if err := c.globalOpts.collection.Create(); err != nil {
			return err
		}
		return nil
	}
	if c.globalOpts.json {
		cmd.Println(c.globalOpts.collection.JSON())
	} else {
		cmd.Println(c.globalOpts.collection)
	}
	return nil
}
