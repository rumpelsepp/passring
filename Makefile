GO ?= go
VERSION := 0.1.1

passring:
	$(GO) build $(GOFLAGS) -ldflags="-X main.version=$(VERSION)" -o $@ .

clean:
	$(RM) passring

.PHONY: passring

