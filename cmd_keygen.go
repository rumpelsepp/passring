package main

import (
	"fmt"
	"os"
	"time"

	"filippo.io/age"
	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh/terminal"
)

type keygenCommand struct {
	globalOpts *globalOptions
	stdout     bool
}

func (c *keygenCommand) run(cmd *cobra.Command, args []string) error {
	var out *os.File
	if c.stdout {
		out = os.Stdout
	} else {
		if _, err := os.Stat(c.globalOpts.keypath); os.IsNotExist(err) {
			if err := os.MkdirAll(c.globalOpts.configdir, 0700); err != nil {
				return err
			}
		}
		f, err := os.OpenFile(c.globalOpts.keypath, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0600)
		if err != nil {
			return err
		}
		defer f.Close()
		out = f
	}

	if fi, err := out.Stat(); err == nil {
		if fi.Mode().IsRegular() && fi.Mode().Perm()&0004 != 0 {
			fmt.Fprintln(os.Stderr, "Warning: writing to a world-readable file.")
			fmt.Fprintln(os.Stderr, "Consider setting the umask to 066 and trying again.")
		}
	}

	k, err := age.GenerateX25519Identity()
	if err != nil {
		return err
	}

	if !terminal.IsTerminal(int(out.Fd())) {
		fmt.Fprintf(os.Stderr, "Public key: %s\n", k.Recipient())
	}

	fmt.Fprintf(out, "# created: %s\n", time.Now().Format(time.RFC3339))
	fmt.Fprintf(out, "# public key: %s\n", k.Recipient())
	fmt.Fprintf(out, "%s\n", k)

	return nil
}
