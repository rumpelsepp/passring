module git.sr.ht/~rumpelsepp/passring

go 1.15

require (
	filippo.io/age v1.0.0-beta5
	github.com/godbus/dbus/v5 v5.0.4-0.20201218172701-b3768b321399
	github.com/pelletier/go-toml v1.2.0
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/twpayne/go-shell v0.3.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf // indirect
)
