package main

import (
	"github.com/spf13/cobra"
)

type rmCommand struct {
	globalOpts *globalOptions
}

func (c *rmCommand) run(cmd *cobra.Command, args []string) error {
	name := args[0]
	item, err := c.globalOpts.collection.Get(name)
	if err != nil {
		return err
	}

	if err := c.globalOpts.collection.DeleteItem(item); err != nil {
		return err
	}
	return nil
}
