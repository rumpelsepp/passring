package passring

import (
	"os"
	"os/exec"
)

type GitWrapper struct {
	Path string
}

func (g *GitWrapper) RunGit(subCmd string, args ...string) error {
	var a []string
	a = append(a, subCmd)
	a = append(a, args...)
	if g.Path != "" {
		if err := os.Chdir(g.Path); err != nil {
			return err
		}
	}
	cmd := exec.Command("git", a...)
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

func (g *GitWrapper) RunGitOutput(subCmd string, args ...string) ([]byte, error) {
	var a []string
	a = append(a, subCmd)
	a = append(a, args...)
	if g.Path != "" {
		if err := os.Chdir(g.Path); err != nil {
			return nil, err
		}
	}
	cmd := exec.Command("git", a...)
	cmd.Stdin = os.Stdin
	output, err := cmd.Output()
	if err != nil {
		return nil, err
	}
	return output, nil
}

func (g *GitWrapper) RunGitCombinedOutput(subCmd string, args ...string) ([]byte, error) {
	var a []string
	a = append(a, subCmd)
	a = append(a, args...)
	if g.Path != "" {
		if err := os.Chdir(g.Path); err != nil {
			return nil, err
		}
	}
	cmd := exec.Command("git", a...)
	cmd.Stdin = os.Stdin
	output, err := cmd.CombinedOutput()
	if err != nil {
		return nil, err
	}
	return output, nil
}

func (g *GitWrapper) IsGitRepo() (bool, error) {
	if _, err := g.RunGitOutput("rev-parse", "--git-dir"); err != nil {
		return false, err
	}
	return true, nil
}

func (g *GitWrapper) IsDirty() (bool, error) {
	output, err := g.RunGitOutput("status", "--porcelain")
	if err != nil {
		return false, err
	}
	if string(output) != "" {
		return true, nil
	}
	return false, nil
}
