package passring

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"filippo.io/age"
)

func DataDir() string {
	p, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	return filepath.Join(p, ".local/share", "passring")
}

func ConfigDir() string {
	p, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}
	return filepath.Join(p, "passring")
}

var re = regexp.MustCompile("[^a-z0-9]+")

// Used for generating valid 'safe' file names.
// Replaces all non printable (lowercase) ASCII chars with '-.
func Slug(s string) string {
	return strings.Trim(re.ReplaceAllString(strings.ToLower(s), "-"), "-")
}

func GetRecipient(identity age.Identity) (age.Recipient, error) {
	switch v := identity.(type) {
	case *age.X25519Identity:
		return v.Recipient(), nil
	}
	return nil, fmt.Errorf("unsupported key type: %T", identity)
}

func SecretFileName(label string) string {
	return fmt.Sprintf("%s.secret", Slug(label))
}
