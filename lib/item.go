package passring

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"time"

	"filippo.io/age"
	"github.com/godbus/dbus/v5"
)

type Secret struct {
	Session     dbus.ObjectPath `json:"-"`
	Parameters  []byte          `json:"parameters"`
	Value       []byte          `json:"value"`
	ContentType string          `json:"content_type"`
}

type Item struct {
	Locked     bool              `json:"-"`
	Attributes map[string]string `json:"attributes"`
	Label      string            `json:"label"`
	Secret     Secret            `json:"secret"`
	Created    uint64            `json:"created"`
	Modified   uint64            `json:"modified"`
}

func NewItem(label string, value []byte, contentType string) *Item {
	timestamp := uint64(time.Now().Unix())
	return &Item{
		Locked:     false,
		Attributes: make(map[string]string),
		Label:      label,
		Secret: Secret{
			Value:       value,
			ContentType: contentType,
		},
		Created:  timestamp,
		Modified: timestamp,
	}
}

func DecryptItem(data []byte, identity age.Identity) ([]byte, error) {
	d, err := age.Decrypt(bytes.NewReader(data), identity)
	if err != nil {
		return nil, err
	}

	rawData, err := ioutil.ReadAll(d)
	if err != nil {
		return nil, err
	}
	return rawData, nil
}

func LoadItem(path string, identity age.Identity) (*Item, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	rawData, err := DecryptItem(data, identity)
	if err != nil {
		return nil, err
	}

	var i Item
	if err := json.Unmarshal(rawData, &i); err != nil {
		return nil, err
	}
	// TODO: Workaround for now.
	if i.Secret.ContentType == "plain/text" {
		i.Secret.ContentType = "text/plain"
	}

	return &i, nil
}

func (i *Item) Encrypt(recipient ...age.Recipient) ([]byte, error) {
	var buf bytes.Buffer
	encWriter, err := age.Encrypt(&buf, recipient...)
	if err != nil {
		return nil, err
	}

	encoder := json.NewEncoder(encWriter)
	if err := encoder.Encode(i); err != nil {
		return nil, err
	}
	if err := encWriter.Close(); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (i *Item) Filename() string {
	return SecretFileName(i.Label)
}

func (i *Item) JSON() string {
	b, err := json.Marshal(i)
	if err != nil {
		panic(err)
	}
	return string(b)
}
