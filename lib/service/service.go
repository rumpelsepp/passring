package service

import (
	"fmt"
	"path"
	"strconv"
	"strings"
	"sync"

	pr "git.sr.ht/~rumpelsepp/passring/lib"
	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/prop"
)

const (
	dbusService             = "org.freedesktop.secrets"
	dbusIFSecretsCollection = "org.freedesktop.Secret.Collection"
	dbusIFSecretsItem       = "org.freedesktop.Secret.Item"
	dbusIFSecretService     = "org.freedesktop.Secret.Service"
	dbusIFSecretsSession    = "org.freedesktop.Secret.Session"
)

const (
	baseObjectPath        = "/org/freedesktop/secrets"
	baseCollObjectPath    = "/org/freedesktop/secrets/collection"
	baseSessionObjectPath = "/org/freedesktop/secrets/session"
	defaultCollObjectPath = "/org/freedesktop/secrets/collection/default"
)

// https://specifications.freedesktop.org/secret-service/latest/re02.html
type DBusCollection struct {
	*pr.Collection
}

func (c *DBusCollection) Delete() *dbus.Error {
	return nil
}

func (c *DBusCollection) SearchItems(fields map[string]string) ([]dbus.ObjectPath, []dbus.ObjectPath, *dbus.Error) {
	fmt.Println("SearchItems called")
	return nil, nil, nil
}

type DBusItem struct {
	pr.Item
	path dbus.ObjectPath
}

func (i *DBusItem) export(conn *dbus.Conn) error {
	propsSpec := map[string]map[string]*prop.Prop{
		dbusIFSecretsItem: {
			"Locked": {
				Value:    i.Locked,
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
			"Attributes": {
				Value:    i.Attributes,
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
			"Label": {
				Value:    i.Label,
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
			"Created": {
				Value:    i.Created,
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
			"Modified": {
				Value:    i.Modified,
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
		},
	}
	if err := conn.Export(i, i.path, dbusIFSecretsItem); err != nil {
		return err
	}
	_, err := prop.Export(conn, i.path, propsSpec)
	if err != nil {
		return err
	}
	return nil
}

func (i *DBusItem) GetSecret(session dbus.ObjectPath) (*pr.Secret, *dbus.Error) {
	// FIXME: fake this for now
	i.Secret.Session = session
	return &i.Secret, nil
}

type SecretService struct {
	Conn *dbus.Conn
	coll DBusCollection

	sessions []*Session
	mutex    sync.Mutex
}

func NewSecretService(coll *pr.Collection) (*SecretService, error) {
	conn, err := dbus.ConnectSessionBus()
	if err != nil {
		return nil, err
	}

	s := &SecretService{Conn: conn}

	reply, err := conn.RequestName(dbusService, dbus.NameFlagDoNotQueue)
	if err != nil {
		return nil, err
	}
	if reply != dbus.RequestNameReplyPrimaryOwner {
		return nil, fmt.Errorf("dbus name already taken")
	}
	if err := s.exportService(); err != nil {
		return nil, err
	}
	if err := s.exportCollection(coll); err != nil {
		return nil, err
	}
	return s, nil
}

func (s *SecretService) exportService() error {
	objPath := dbus.ObjectPath(baseObjectPath)
	if err := s.Conn.Export(s, objPath, dbusIFSecretService); err != nil {
		return err
	}

	// Properties
	propsSpec := map[string]map[string]*prop.Prop{
		dbusIFSecretService: {
			"Collections": {
				Value:    []dbus.ObjectPath{defaultCollObjectPath},
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
			"DefaultCollection": {
				Value:    "default",
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
		},
	}
	_, err := prop.Export(s.Conn, baseObjectPath, propsSpec)
	if err != nil {
		return err
	}
	return nil
}

func (s *SecretService) exportItems() ([]dbus.ObjectPath, error) {
	var paths []dbus.ObjectPath
	for i, item := range s.coll.Items {
		itemPath := dbus.ObjectPath(path.Join(baseCollObjectPath, strconv.Itoa(i)))
		paths = append(paths, itemPath)
		item.Secret.Session = dbus.ObjectPath(path.Join(baseSessionObjectPath, "0"))
		dbusItem := DBusItem{
			path: itemPath,
			Item: *item,
		}
		if err := dbusItem.export(s.Conn); err != nil {
			return nil, err
		}
	}
	return paths, nil
}

func (s *SecretService) exportCollection(coll *pr.Collection) error {
	s.coll = DBusCollection{Collection: coll}
	objPath := dbus.ObjectPath(path.Join(baseCollObjectPath, s.coll.Label))
	if err := s.Conn.Export(s.coll, objPath, dbusIFSecretsCollection); err != nil {
		return err
	}

	itemPaths, err := s.exportItems()
	if err != nil {
		return err
	}

	propsSpec := map[string]map[string]*prop.Prop{
		dbusIFSecretsCollection: {
			"Items": {
				Value:    itemPaths,
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
			"Label": {
				Value:    s.coll.Label, // TODO: implement
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
			"Locked": {
				Value:    false, // TODO: implement
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
			"Created": {
				Value:    s.coll.Created,
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
			"Modified": {
				Value:    s.coll.Modified,
				Writable: false,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
		},
	}
	_, err = prop.Export(s.Conn, objPath, propsSpec)
	if err != nil {
		return err
	}
	return nil
}

func (s *SecretService) OpenSession(algorithm string, input dbus.Variant) (dbus.Variant, dbus.ObjectPath, *dbus.Error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	// TODO: Implement fancy crypto.
	if algorithm != "plain" {
		return dbus.Variant{}, "", dbus.NewError("org.freedesktop.DBus.Error.NotSupported", nil)
	}
	var (
		sess = &Session{}
		id   = strconv.Itoa(len(s.sessions))
	)

	objPath := dbus.ObjectPath(path.Join(baseSessionObjectPath, id))
	if err := s.Conn.Export(sess, objPath, dbusIFSecretsSession); err != nil {
		return dbus.Variant{}, "", dbus.MakeFailedError(fmt.Errorf("session creation failed"))
	}

	s.sessions = append(s.sessions, sess)
	return dbus.MakeVariant(""), objPath, nil
}

func (s *SecretService) CreateCollection(label string, private bool) *dbus.Error {
	fmt.Println("CreateCollection called")
	return nil
}

func (s *SecretService) SearchItems(attributes map[string]string) ([]dbus.ObjectPath, []dbus.ObjectPath, *dbus.Error) {
	var out []dbus.ObjectPath
	for k, v := range attributes {
		items, err := s.coll.List()
		if err != nil {
			return nil, nil, dbus.MakeFailedError(err)
		}
		for i, item := range items {
			// TODO: only label is supported
			if strings.ToLower(k) == "label" {
				if item.Label == v {
					out = append(out, dbus.ObjectPath(path.Join(baseObjectPath, strconv.Itoa(i))))
					return out, nil, nil
				}
			}
		}
	}
	return nil, nil, nil
}

func (s *SecretService) Lock() *dbus.Error {
	fmt.Println("LockService called")
	return nil
}

func (s *SecretService) Unlock() *dbus.Error {
	fmt.Println("UnLockService called")
	return nil
}

func (s *SecretService) ReadAlias(name string) (dbus.ObjectPath, *dbus.Error) {
	if name == "default" {
		return dbus.ObjectPath(defaultCollObjectPath), nil
	}
	return "/", nil
}

func (s *SecretService) GetSecrets(itemPaths []dbus.ObjectPath, session dbus.ObjectPath) (map[dbus.ObjectPath]pr.Secret, *dbus.Error) {
	items, err := s.coll.List()
	if err != nil {
		return nil, dbus.MakeFailedError(err)
	}
	out := make(map[dbus.ObjectPath]pr.Secret)
	for _, itemPath := range itemPaths {
		indexStr := path.Base(string(itemPath))
		index, err := strconv.Atoi(indexStr)
		if err != nil {
			return nil, dbus.MakeFailedError(err)
		}
		out[itemPath] = items[index].Secret
	}
	return out, nil
}

type Session struct {
}

func (s *Session) Close() *dbus.Error {
	fmt.Println("Session Close() called")
	return nil
}
