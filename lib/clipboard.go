package passring

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func ClipboardSet(data string) error {
	var (
		cmd        *exec.Cmd
		candidates = []string{"wl-copy", "xsel"}
	)
	for _, c := range candidates {
		if _, err := exec.LookPath(c); err == nil {
			cmd = exec.Command(c)
		}
	}
	if cmd == nil {
		return fmt.Errorf("no clipboard tool installed")
	}

	cmd.Stdin = strings.NewReader(data)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}
