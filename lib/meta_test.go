package passring

import (
	"strings"
	"testing"
)

var data = `
Foo:bar
baz:123
hans:sepp ist cool
`

func TestParseMetadata(t *testing.T) {
	_, err := ParseMetadata(data)
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetInt(t *testing.T) {
	m, err := ParseMetadata(data)
	if err != nil {
		t.Fatal(err)
	}

	a, err := m.GetInt("baz")
	if err != nil {
		t.Fatal(err)
	}

	if a != 123 {
		t.Fatal("wrong int value for baz")
	}

	_, err = m.GetInt("hans")
	if err == nil {
		t.Fatal("error handling is borked")
	}

	_, err = m.GetInt("blabla")
	if err != ErrNotExists {
		t.Fatal("error handling is borked")
	}
}

func TestGetString(t *testing.T) {
	m, err := ParseMetadata(data)
	if err != nil {
		t.Fatal(err)
	}

	a, err := m.GetString("Foo")
	if err != nil {
		t.Fatal(err)
	}

	if a != "bar" {
		t.Fatal("wrong string value for Foo")
	}

	b, err := m.GetString("hans")
	if err != nil {
		t.Fatal(err)
	}

	if b != "sepp ist cool" {
		t.Fatal("wrong string value for hans")
	}

	_, err = m.GetInt("blabla")
	if err != ErrNotExists {
		t.Fatal("error handling is borked")
	}
}

func TestString(t *testing.T) {
	m, err := ParseMetadata(data)
	if err != nil {
		t.Fatal(err)
	}

	if s := m.String(); s != strings.TrimSpace(data) {
		t.Fatal("String() is borked")
	}
}
