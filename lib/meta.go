package passring

import (
	"bufio"
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type Metadata map[string]string

var (
	ErrInvalidData = errors.New("invalid data")
	ErrNotExists   = errors.New("no such value")
)

func ParseMetadata(data string) (Metadata, error) {
	var (
		out     = make(map[string]string)
		scanner = bufio.NewScanner(strings.NewReader(data))
	)
	for scanner.Scan() {
		t := strings.TrimSpace(scanner.Text())
		if t == "" || strings.HasPrefix(t, "#") {
			continue
		}
		parts := strings.SplitN(t, ":", 2)
		if len(parts) != 2 {
			return nil, ErrInvalidData
		}
		out[parts[0]] = parts[1]
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return Metadata(out), nil
}

func (m Metadata) GetString(key string) (string, error) {
	if v, ok := m[key]; ok {
		return v, nil
	}
	return "", ErrNotExists
}

func (m Metadata) GetInt(key string) (int, error) {
	if v, ok := m[key]; ok {
		return strconv.Atoi(v)
	}
	return 0, ErrNotExists
}

func (m Metadata) String() string {
	var b strings.Builder
	for k, v := range m {
		b.WriteString(fmt.Sprintf("%s:%s\n", k, v))
	}
    return strings.TrimSpace(b.String())
}
