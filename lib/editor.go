package passring

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
)

func EditInteractive(data []byte) ([]byte, error) {
	editor := os.Getenv("EDITOR")
	if editor == "" {
		editor = "nano"
	}
	f, err := ioutil.TempFile("", "passring")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	if _, err := io.Copy(f, bytes.NewReader(data)); err != nil {
		return nil, err
	}

	cmd := exec.Command(editor, f.Name())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	if err := cmd.Run(); err != nil {
		return nil, err
	}
	out, err := ioutil.ReadFile(f.Name())
	if err != nil {
		return nil, err
	}
	return out, nil
}
