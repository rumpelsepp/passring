package passring

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"filippo.io/age"
)

const metaFilename = "META"

type Collection struct {
	metadata Metadata
	Path     string
	Items    []*Item
	Label    string
	Locked   bool
	Created  uint64
	Modified uint64
}

func NewCollection(label string) *Collection {
	collPath := filepath.Join(DataDir(), label)
	return &Collection{Label: label, Path: collPath}
}

func (c *Collection) UpdateTimestamps() error {
	var (
		metaPath          = filepath.Join(c.Path, metaFilename)
		metadata Metadata = make(map[string]string)
		now               = uint64(time.Now().Unix())
		nowStr            = strconv.Itoa(int(now))
	)
	if c.Created == 0 {
		c.Created = now
		metadata["Created"] = nowStr
	} else {
		metadata["Created"] = strconv.Itoa(int(c.Created))
	}
	c.Modified = now
	metadata["Modified"] = nowStr
	if err := ioutil.WriteFile(metaPath, []byte(metadata.String()), 0600); err != nil {
		return err
	}
	return nil
}

func (c *Collection) Create() error {
	if _, err := os.Stat(c.Path); err == nil {
		return fmt.Errorf("collection '%s' already exists", c.Label)
	}
	if err := os.MkdirAll(c.Path, 0700); err != nil {
		return err
	}
	f, err := os.Create(filepath.Join(c.Path, metaFilename))
	if err != nil {
		return err
	}
	if err := f.Close(); err != nil {
		return err
	}
	if err := c.UpdateTimestamps(); err != nil {
		return err
	}
	return nil
}

func (c *Collection) Delete() error {
	return os.RemoveAll(c.Path)
}

func (c *Collection) Load(privKey age.Identity) error {
	metaPath := filepath.Join(c.Path, metaFilename)
	m, err := ioutil.ReadFile(metaPath)
	if err != nil {
		return err
	}
	metadata, err := ParseMetadata(string(m))
	if err != nil {
		return err
	}
	c.metadata = metadata

	if v, err := metadata.GetInt("Created"); err != nil {
		return err
	} else {
		c.Created = uint64(v)
	}
	if v, err := metadata.GetInt("Modified"); err != nil {
		return err
	} else {
		c.Modified = uint64(v)
	}

	files, err := filepath.Glob(filepath.Join(c.Path, "*.secret"))
	if err != nil {
		return err
	}
	for _, file := range files {
		stat, err := os.Stat(file)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			continue
		}
		if stat.IsDir() {
			continue
		}
		item, err := LoadItem(file, privKey)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			continue
		}
		c.Items = append(c.Items, item)
	}
	return nil
}

func (c *Collection) List() ([]*Item, error) {
	if len(c.Items) == 0 {
		return nil, fmt.Errorf("no secrets available")
	}
	return c.Items, nil
}

func (c *Collection) Get(label string) (*Item, error) {
	for _, item := range c.Items {
		if item.Label == label {
			return item, nil
		}
	}
	return nil, fmt.Errorf("no such secret: %s", label)
}

func (c *Collection) Set(item *Item, recipient ...age.Recipient) error {
	p := filepath.Join(c.Path, item.Filename())
	if _, err := os.Stat(p); err == nil {
		return fmt.Errorf("secret exists")
	}
	data, err := item.Encrypt(recipient...)
	if err != nil {
		return err
	}
	if err := c.UpdateTimestamps(); err != nil {
		return err
	}
	return ioutil.WriteFile(p, data, 0600)
}

func (c *Collection) DeleteItem(item *Item) error {
	p := filepath.Join(c.Path, item.Filename())
	err := os.RemoveAll(p)
	if err != nil {
		return nil
	}
	if err := c.UpdateTimestamps(); err != nil {
		return err
	}
	return nil
}

func (c *Collection) JSON() string {
	b, err := json.Marshal(c)
	if err != nil {
		panic(err)
	}
	return string(b)
}
