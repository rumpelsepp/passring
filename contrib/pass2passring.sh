#!/bin/bash

set -eu

PASSRING="${PASSRING:-passring}"
PASSSTORE="${PASSSTORE:-$HOME/.password-store}"

cd "$PASSSTORE"
for f in *.gpg */**.gpg */*/**.gpg; do
    name_with="${f%%.gpg}"
    name_without="${name_with//\//-}"
    echo "importing $name_with"
    pass show "$name_with" 2>/dev/null | $PASSRING new -s "$name_without"
done
