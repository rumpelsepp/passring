package main

import (
	"github.com/spf13/cobra"
)

type listCommand struct {
	globalOpts *globalOptions
}

func (c *listCommand) run(cmd *cobra.Command, args []string) error {
	items, err := c.globalOpts.collection.List()
	if err != nil {
		return err
	}
	for _, item := range items {
		cmd.Println(item.Label)
	}
	return nil
}
