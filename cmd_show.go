package main

import (
	"fmt"
	"strings"

	pr "git.sr.ht/~rumpelsepp/passring/lib"
	"github.com/spf13/cobra"
)

type showCommand struct {
	globalOpts *globalOptions
	clipboard  bool
	json       bool
}

func (c *showCommand) run(cmd *cobra.Command, args []string) error {
	if len(args) != 1 {
		return fmt.Errorf("secret name is missing")
	}

	name := args[0]
	item, err := c.globalOpts.collection.Get(name)
	if err != nil {
		return err
	}

	if c.globalOpts.json {
		fmt.Println(item.JSON())
	} else {
		data := strings.TrimSpace(string(item.Secret.Value))
		if c.clipboard {
			pr.ClipboardSet(data)
		} else {
			cmd.Println(data)
		}
	}
	return nil
}
