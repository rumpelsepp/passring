package main

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"

	"filippo.io/age"
	pr "git.sr.ht/~rumpelsepp/passring/lib"
	toml "github.com/pelletier/go-toml"
	"github.com/spf13/cobra"
	shell "github.com/twpayne/go-shell"
	"golang.org/x/sys/unix"
)

var version string

type config struct {
	Recipients []string
}

type globalOptions struct {
	config     *config
	privateKey age.Identity
	recipients []age.Recipient
	collection *pr.Collection

	datadir    string
	configdir  string
	configpath string
	keypath    string
	collName   string
	json       bool
	git        bool
}

func main() {
	var (
		configdir  = pr.ConfigDir()
		globalOpts = globalOptions{
			datadir:    pr.DataDir(),
			configdir:  configdir,
			configpath: filepath.Join(configdir, "config.toml"),
			keypath:    filepath.Join(configdir, "key.txt"),
		}
		catCmd        = catCommand{globalOpts: &globalOpts}
		collectionCmd = collectionCommand{globalOpts: &globalOpts}
		daemonCmd     = daemonCommand{globalOpts: &globalOpts}
		editCmd       = editCommand{globalOpts: &globalOpts}
		keygenCmd     = keygenCommand{globalOpts: &globalOpts}
		listCmd       = listCommand{globalOpts: &globalOpts}
		newCmd        = newCommand{globalOpts: &globalOpts}
		mvCmd         = mvCommand{globalOpts: &globalOpts}
		rmCmd         = rmCommand{globalOpts: &globalOpts}
		showCmd       = showCommand{globalOpts: &globalOpts}
	)

	// completion closures
	completeSecret := func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if len(args) != 0 {
			return nil, cobra.ShellCompDirectiveNoFileComp
		}
		items, err := globalOpts.collection.List()
		if err != nil {
			return nil, cobra.ShellCompDirectiveError
		}
		var out []string
		for _, s := range items {
			out = append(out, s.Label)
		}
		return out, cobra.ShellCompDirectiveNoFileComp
	}
	// git commit helper
	gitCommit := func(cmd *cobra.Command, args []string) error {
		if !globalOpts.git {
			return nil
		}
		git := pr.GitWrapper{Path: globalOpts.collection.Path}
		isRepo, err := git.IsGitRepo()
		if err != nil {
			return err
		}
		if !isRepo {
			return nil
		}
		dirty, err := git.IsDirty()
		if err != nil {
			return err
		}
		if !dirty {
			return nil
		}
		if err := git.RunGit("add", "."); err != nil {
			return err
		}
		// TODO: Improve commit message
		if err := git.RunGit("commit", "-m", "autocommit"); err != nil {
			return err
		}
		return nil
	}

	var (
		rootCobraCmd = &cobra.Command{
			Use:          "passring",
			Short:        "passring",
			SilenceUsage: true,
			PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
				switch cmd.CalledAs() {
				case "collection", "keygen", "version":
					return nil
				}
				// Read the config if it is available.
				if _, err := os.Stat(globalOpts.configpath); err == nil {
					c := config{}
					if data, err := ioutil.ReadFile(globalOpts.configpath); err != nil {
						if err := toml.Unmarshal(data, &c); err != nil {
							cmd.PrintErrln(err)
						} else {
							globalOpts.config = &c
						}
					}
				}
				// Parse the private key.
				rawKey, err := ioutil.ReadFile(globalOpts.keypath)
				if err != nil {
					return err
				}
				key, err := age.ParseIdentities(bytes.NewReader(rawKey))
				if err != nil {
					return err
				}
				globalOpts.privateKey = key[0]
				// If a config is there, parse the public keys.
				// Else use the own recipient.
				var r []age.Recipient
				if globalOpts.config != nil {
					for _, raw := range globalOpts.config.Recipients {
						rec, err := age.ParseX25519Recipient(string(raw))
						if err != nil {
							return err
						}
						r = append(r, rec)
					}
				} else {
					rec, err := pr.GetRecipient(globalOpts.privateKey)
					if err != nil {
						return err
					}
					r = append(r, rec)
				}
				globalOpts.recipients = r

				c := pr.NewCollection(globalOpts.collName)
				if err := c.Load(key[0]); err != nil {
					return err
				}

				globalOpts.privateKey = key[0]
				globalOpts.collection = c
				return nil
			},
		}
		catCobraCmd = &cobra.Command{
			Use:               "cat",
			Short:             "Show raw passring files decrypted",
			RunE:              catCmd.run,
			ValidArgsFunction: completeSecret,
		}
		cdCobraCmd = &cobra.Command{
			Use:   "cd",
			Short: "Spawn an new shell with CWD in the collection directory",
			RunE: func(cmd *cobra.Command, args []string) error {
				shellCmd, _ := shell.CurrentUserShell()

				if err := os.Chdir(globalOpts.collection.Path); err != nil {
					return err
				}
				if err := unix.Exec(shellCmd, []string{shellCmd}, os.Environ()); err != nil {
					return err
				}
				// Never reached, but needed for the typechecking.
				return nil
			},
		}
		collectionCobraCmd = &cobra.Command{
			Use:   "collection",
			Short: "Manage collections",
			RunE:  collectionCmd.run,
		}
		completionCobraCmd = &cobra.Command{
			Use:                   "completion",
			Short:                 "Generate shell completion scripts",
			DisableFlagsInUseLine: true,
			ValidArgs:             []string{"bash", "zsh", "fish"},
			Args:                  cobra.ExactValidArgs(1),
			Run: func(cmd *cobra.Command, args []string) {
				switch args[0] {
				case "bash":
					cmd.Root().GenBashCompletion(os.Stdout)
				case "zsh":
					cmd.Root().GenZshCompletion(os.Stdout)
				case "fish":
					cmd.Root().GenFishCompletion(os.Stdout, true)
				}
			},
		}
		daemonCobraCmd = &cobra.Command{
			Use:   "daemon",
			Short: "Run as a fd-secret service",
			RunE:  daemonCmd.run,
		}
		editCobraCmd = &cobra.Command{
			Use:               "edit",
			Short:             "Generate a new key",
			RunE:              editCmd.run,
			PostRunE:          gitCommit,
			ValidArgsFunction: completeSecret,
		}
		keygenCobraCmd = &cobra.Command{
			Use:   "keygen",
			Short: "Generate a new key",
			RunE:  keygenCmd.run,
		}
		listCobraCmd = &cobra.Command{
			Use:   "list",
			Short: "List the secrets in the collection",
			RunE:  listCmd.run,
		}
		mvCobraCmd = &cobra.Command{
			Use:      "mv",
			Short:    "Rename a secret",
			RunE:     mvCmd.run,
			PostRunE: gitCommit,
		}
		newCobraCmd = &cobra.Command{
			Use:      "new",
			Short:    "Create a new secret",
			RunE:     newCmd.run,
			PostRunE: gitCommit,
		}
		showCobraCmd = &cobra.Command{
			Use:               "show",
			Short:             "Show a secret",
			RunE:              showCmd.run,
			ValidArgsFunction: completeSecret,
		}
		rmCobraCmd = &cobra.Command{
			Use:               "rm",
			Short:             "Delete a secret",
			RunE:              rmCmd.run,
			PostRunE:          gitCommit,
			ValidArgsFunction: completeSecret,
		}
		versionCobraCmd = &cobra.Command{
			Use:   "version",
			Short: "Show version and exit",
			Run: func(cmd *cobra.Command, args []string) {
				cmd.Printf("passring %s\n", version)
			},
		}
	)

	// global
	globalFlags := rootCobraCmd.PersistentFlags()
	globalFlags.StringVar(&globalOpts.collName, "collection", "default", "Choose collection")
	globalFlags.BoolVarP(&globalOpts.json, "json", "j", false, "Print data as json")
	globalFlags.BoolVarP(&globalOpts.git, "git", "g", false, "Do git commits automatically")
	globalFlags.StringVarP(&globalOpts.keypath, "key", "k", globalOpts.keypath, "Path to the private key")

	// cat
	rootCobraCmd.AddCommand(catCobraCmd)

	// cd
	rootCobraCmd.AddCommand(cdCobraCmd)

	// collection
	rootCobraCmd.AddCommand(collectionCobraCmd)
	collectionFlags := collectionCobraCmd.Flags()
	collectionFlags.BoolVarP(&collectionCmd.init, "init", "i", false, "Initialize collection")

	// completion
	rootCobraCmd.AddCommand(completionCobraCmd)

	// daemon
	rootCobraCmd.AddCommand(daemonCobraCmd)

	// edit
	rootCobraCmd.AddCommand(editCobraCmd)

	// keygen
	rootCobraCmd.AddCommand(keygenCobraCmd)
	keygenFlags := keygenCobraCmd.Flags()
	keygenFlags.BoolVarP(&keygenCmd.stdout, "stdout", "s", false, "Print key to stdout")

	// list
	rootCobraCmd.AddCommand(listCobraCmd)

	// mv
	rootCobraCmd.AddCommand(mvCobraCmd)

	// new
	rootCobraCmd.AddCommand(newCobraCmd)
	newFlags := newCobraCmd.Flags()
	newFlags.BoolVarP(&newCmd.stdin, "stdin", "s", false, "Read data from stdin")

	// rm
	rootCobraCmd.AddCommand(rmCobraCmd)

	// show
	rootCobraCmd.AddCommand(showCobraCmd)
	showFlags := showCobraCmd.Flags()
	showFlags.BoolVarP(&showCmd.clipboard, "clipboard", "c", false, "Put secret in the system clipboard")

	// version
	rootCobraCmd.AddCommand(versionCobraCmd)

	// Wire everything up.
	rootCobraCmd.Execute()
}
