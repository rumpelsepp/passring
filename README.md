# passring

passring is an experimental password manager similar to [pass](https://www.passwordstore.org/).
passring follows an even simpler approach as pass does.
It uses [age](https://age-encryption.org/) for encrypting the secrets instead of pgp.
This simplifies the private key handling a lot.
passring is declared as experimental, since age does not support encrypting the private key.
This is on the [roadmap](https://github.com/FiloSottile/age/issues/86) and the hint will be removed once this feature is available upstream.

A target for this project is implementing the [SecretService specification](https://specifications.freedesktop.org/secret-service/latest/).
This means that `passring` will be usable as a drop in replacement for [`gnome-keyring`](https://wiki.gnome.org/Projects/GnomeKeyring/) once its done.
Eventually this will enable a fully integrated keyring to your desktop which can be syncronized easily (e.g. via Git) to multiple machines.

## Getting Started

Generate a key:

```
$ passring keygen
```

```
$ passring collection --init default
```

Add a secret:

```
$ passring new fancy-service
```

Show a secret:

```
$ passring show fancy-service
```

Change to the secret directory and do some `git` magic:

```
$ passring cd
$ git commit -a
```

Tab completion is supported for bash, fish, and zsh.
Generating shell completion scripts for e.g. `fish`:

```
$ passring completion fish | source
```

For using multiple keys (e.g. one key for each computer) create a config file at `$HOME/.config/passring/config.toml`:

```
# You need to add all keys!
recipients = ["age1fq2xlv0vdfarjjluzw9tfducxy4td7ayyvyjan3ycwz2a5znu34ssehhz4"]
```

## Architecture

The private key is stored in `$HOME/.config/passring/key.txt`.
Secrets are grouped in collections and stored in `$HOME/.local/share/passring/COLLECTION`.
The default collection is called `default`.

Each secret is stored in a JSON file with the file ending `.secret` and encrypted with the age algorithm.
Multiple recipients (aka multitple different private keys) for managing secrets on different machines are supported.
The datastructure of a secret is taken from the [secret service specification](https://specifications.freedesktop.org/secret-service/latest/ch14.html#type-Secret).

## Differences from pass

- [age](https://age-encryption.org) is used for encrypting secretes instead of pgp.
  It simplifies key handling a lot.
- The commandline interface aims to be more suitable for scripts.
  For instance, json output everywhere.
- Eventually the SecretService specification will be implemented.
  This enables full integration into a desktop environment such as gnome.
