package main

import (
	"fmt"
	"io/ioutil"
	"os"

	pr "git.sr.ht/~rumpelsepp/passring/lib"
	"github.com/spf13/cobra"
)

type newCommand struct {
	globalOpts *globalOptions
	comment    string
	stdin      bool
}

func (c *newCommand) run(cmd *cobra.Command, args []string) error {
	if len(args) != 1 {
		return fmt.Errorf("secret is missing a name")
	}

	datadir := c.globalOpts.datadir
	if _, err := os.Stat(datadir); os.IsNotExist(err) {
		if err := os.MkdirAll(datadir, 0700); err != nil {
			return err
		}
	}

	var (
		err  error
		data []byte
	)
	if c.stdin {
		data, err = ioutil.ReadAll(os.Stdin)
	} else {
		data, err = pr.EditInteractive([]byte(""))
	}
	if err != nil {
		return err
	}
	item := pr.NewItem(args[0], data, "text/plain")

	if err := c.globalOpts.collection.Set(item, c.globalOpts.recipients...); err != nil {
		return err
	}
	return nil
}
