package main

import (
	"fmt"

	pr "git.sr.ht/~rumpelsepp/passring/lib"
	"github.com/spf13/cobra"
)

type editCommand struct {
	globalOpts *globalOptions
}

func (c *editCommand) run(cmd *cobra.Command, args []string) error {
	if len(args) != 1 {
		return fmt.Errorf("secret name is missing")
	}

	name := pr.Slug(args[0])
	item, err := c.globalOpts.collection.Get(name)
	if err != nil {
		return err
	}

	data, err := pr.EditInteractive(item.Secret.Value)
	if err != nil {
		return err
	}

	item.Secret.Value = data

	if err := c.globalOpts.collection.Set(item, c.globalOpts.recipients...); err != nil {
		return err
	}
	return nil
}
