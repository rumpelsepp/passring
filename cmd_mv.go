package main

import (
	"github.com/spf13/cobra"
)

type mvCommand struct {
	globalOpts *globalOptions
}

func (c *mvCommand) run(cmd *cobra.Command, args []string) error {
	oldName := args[0]
	newName := args[1]
	item, err := c.globalOpts.collection.Get(oldName)
	if err != nil {
		return err
	}

	if err := c.globalOpts.collection.DeleteItem(item); err != nil {
		return err
	}
	item.Label = newName
	if err := c.globalOpts.collection.Set(item, c.globalOpts.recipients...); err != nil {
		return err
	}
	return nil
}
