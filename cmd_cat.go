package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	pr "git.sr.ht/~rumpelsepp/passring/lib"
	"github.com/spf13/cobra"
)

type catCommand struct {
	globalOpts *globalOptions
}

func (c *catCommand) run(cmd *cobra.Command, args []string) error {
	if len(args) != 1 {
		return fmt.Errorf("secret name is missing")
	}

	var (
		filename = pr.SecretFileName(args[0])
		path     = filepath.Join(c.globalOpts.collection.Path, filename)
	)
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	s, err := pr.DecryptItem(data, c.globalOpts.privateKey)
	if err != nil {
		return err
	}

	cmd.Println(strings.TrimSpace(string(s)))
	return nil
}
